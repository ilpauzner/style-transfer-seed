import imghdr
import io
import os
import time

import boto3
import cv2 as cv
import numpy as np
import ydb


def predict(net, img, h, w):
    blob = cv.dnn.blobFromImage(img, 1.0, (w, h),
                                (103.939, 116.779, 123.680), swapRB=False, crop=False)

    print('[INFO] Setting the input to the model')
    net.setInput(blob)

    print('[INFO] Starting Inference!')
    start = time.time()
    out = net.forward()
    end = time.time()
    print('[INFO] Inference Completed successfully!')

    # Reshape the output tensor and add back in the mean subtraction, and
    # then swap the channel ordering
    out = out.reshape((3, out.shape[2], out.shape[3]))
    out[0] += 103.939
    out[1] += 116.779
    out[2] += 123.680
    out /= 255.0
    out = out.transpose(1, 2, 0)

    # Printing the inference time
    print('[INFO] The model ran in {:.4f} seconds'.format(end - start))

    return out


# Source for this function:
# https://github.com/jrosebr1/imutils/blob/4635e73e75965c6fef09347bead510f81142cf2e/imutils/convenience.py#L65
def resize_img(img, width=None, height=None, inter=cv.INTER_AREA):
    dim = None
    h, w = img.shape[:2]

    if width is None and height is None:
        return img
    elif width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    elif height is None:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv.resize(img, dim, interpolation=inter)
    return resized


def process_image(img, model):
    net = cv.dnn.readNetFromTorch(model)
    h, w = img.shape[:2]
    out = predict(net, img, h, w)
    out = cv.convertScaleAbs(out, alpha=255.0)
    return out


def handler(event, context):
    d = event['messages'][0]["details"]
    uuid = d['message']['body']

    s3 = boto3.client(service_name='s3', endpoint_url='https://storage.yandexcloud.net')
    with io.BytesIO() as f:
        s3.download_fileobj(os.getenv('INPUT_BUCKET_NAME'), uuid, f)
        results = f.getvalue()
        ext = imghdr.what(None, h=results)
        img = cv.imdecode(np.frombuffer(results, dtype=np.uint8), cv.IMREAD_COLOR)
        img = resize_img(img, width=600)

    driver_config = ydb.DriverConfig(
        "grpcs://ydb.serverless.yandexcloud.net:2135",
        "/ru-central1/b1gajsrutubnqta5an5d/etn02s0o1gv2uipb77pf",
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
            exit(1)

        model_name = driver.table_client.session().create().transaction(ydb.SerializableReadWrite()).execute(
            f'SELECT Model FROM tasks WHERE Id = "{uuid}";',
            commit_tx=True,
        )[0].rows[0]["Model"]
        model = f'models/{model_name}.t7'

        out = process_image(img, model)
        s3.put_object(Body=cv.imencode('.' + ext, out)[1].tobytes(), Bucket=os.getenv('OUTPUT_BUCKET_NAME'), Key=uuid)
        url = s3.generate_presigned_url('get_object',
                                        Params={'Bucket': os.getenv('OUTPUT_BUCKET_NAME'),
                                                'Key': uuid},
                                        ExpiresIn=60 * 60 * 6)
        s3.delete_object(Bucket=os.getenv('INPUT_BUCKET_NAME'), Key=uuid)

        driver.table_client.session().create().transaction(ydb.SerializableReadWrite()).execute(
            f'UPDATE tasks SET Status = "DONE", Url = "{url}"  WHERE Id = "{uuid}";',
            commit_tx=True,
        )

    return {
        'statusCode': 200,
        'body': 'Hello World!',
    }
