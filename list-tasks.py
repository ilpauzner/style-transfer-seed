import os

import ydb


def handler(event, context):
    driver_config = ydb.DriverConfig(
        "grpcs://ydb.serverless.yandexcloud.net:2135",
        os.getenv("YDB_DATABASE"),
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
            exit(1)

        body = driver.table_client.session().create().transaction(ydb.SerializableReadWrite()).execute(
            'SELECT Id FROM tasks;',
            commit_tx=True,
        )

    return {
        'statusCode': 200,
        'body': body[0].rows,
        'headers': {
            'Content-Type': 'application/json'
        },
    }
