import imghdr
import io
import os
from concurrent.futures import TimeoutError

import boto3
import ydb


def handler(event, context):
    d = event['messages'][0]["details"]
    bucket_id = d["bucket_id"]
    object_id = d["object_id"]

    s3 = boto3.client(service_name='s3', endpoint_url='https://storage.yandexcloud.net')
    sqs = boto3.client(service_name='sqs', endpoint_url='https://message-queue.api.cloud.yandex.net')

    driver_config = ydb.DriverConfig(
        "grpcs://ydb.serverless.yandexcloud.net:2135",
        os.getenv("YDB_DATABASE"),
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )

    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
            exit(1)

        with io.BytesIO() as f:
            s3.download_fileobj(bucket_id, object_id, f)
            results = f.getvalue()

            what = imghdr.what(None, h=results)
            size = len(results)
            print(what, size)

            status = "REJECTED"
            if what is not None and size < 10 ** 7:
                sqs.send_message(
                    QueueUrl=os.getenv('QUEUE_URL'),
                    MessageBody=object_id,
                )
                status = "PROCESSING"

            driver.table_client.session().create().transaction(ydb.SerializableReadWrite()).execute(
                f'UPDATE tasks SET Status = "{status}" WHERE Id = "{object_id}";',
                commit_tx=True,
            )

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': "Empty",
    }
