from concurrent.futures import TimeoutError
from os import getenv
from uuid import uuid4

import boto3
import ydb


def f(model):
    driver_config = ydb.DriverConfig(
        "grpcs://ydb.serverless.yandexcloud.net:2135",
        getenv("YDB_DATABASE"),
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
            exit(1)

        uuid = uuid4().hex

        driver.table_client.session().create().transaction(ydb.SerializableReadWrite()).execute(
            f'INSERT INTO tasks (Id, Model, Status, Url) VALUES ("{uuid}", "{model}", "NEW", "");',
            commit_tx=True,
        )

        session = boto3.session.Session()
        s3 = session.client(service_name='s3', endpoint_url=getenv('AWS_ENDPOINT'))
        bucket_name = getenv('BUCKET_NAME')
        return s3.generate_presigned_post(bucket_name, uuid)


def handler(event, context):
    model = event['queryStringParameters']['model']
    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'isBase64Encoded': False,
        'body': f(model)
    }
