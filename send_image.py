import sys

import requests

response = requests.post(f'https://d5dg3gukegj3cslu88m7.apigw.yandexcloud.net/create_task?model={sys.argv[2]}').json()

with open(sys.argv[1], 'rb') as f:
    files = {'file': (sys.argv[1], f)}
    http_response = requests.post(response['url'], data=response['fields'], files=files)
    print(http_response)
